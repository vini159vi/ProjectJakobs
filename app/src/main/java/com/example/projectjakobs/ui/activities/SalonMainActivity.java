package com.example.projectjakobs.ui.activities;

import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.ActivitySalonMainBinding;

import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

public class SalonMainActivity extends AppCompatActivity {

    ActivitySalonMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_salon_main);

        NavigationUI.setupWithNavController(binding.navigationSalon, Navigation.findNavController(this, R.id.nav_host_fragment));
    }
}
