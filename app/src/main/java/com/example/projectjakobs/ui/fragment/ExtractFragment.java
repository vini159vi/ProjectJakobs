package com.example.projectjakobs.ui.fragment;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.FragmentExtractBinding;
import com.example.projectjakobs.domain.NumberOfPersonsByAgeRange;
import com.example.projectjakobs.domain.NumberOfPersonsWithSpecificGender;
import com.example.projectjakobs.domain.NumberOfTransactionsOnDay;
import com.example.projectjakobs.impl.ExtractFragmnetImpl;
import com.example.projectjakobs.util.Constants;
import com.example.projectjakobs.util.SharedPreferencesUtil;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.model.GradientColor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class ExtractFragment extends Fragment {

    private FragmentExtractBinding binding;
    private ExtractFragmnetImpl impl = new ExtractFragmnetImpl();

    public ExtractFragment() {
    }


    public static ExtractFragment newInstance(String param1, String param2) {
        ExtractFragment fragment = new ExtractFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_extract, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        impl.getSchedules(SharedPreferencesUtil.getSalon(getContext()).getCnpj())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(objects -> {
                    List<NumberOfTransactionsOnDay> numberOfTransactionsOnDays = new ArrayList<>();
                    List<NumberOfPersonsByAgeRange> numberOfPersonsByAgeRanges = new ArrayList<>();
                    List<NumberOfPersonsWithSpecificGender> numberOfPersonsWithSpecificGenderList = new ArrayList<>();

                    for (Object o : objects) {
                        if (o instanceof NumberOfTransactionsOnDay) {
                            numberOfTransactionsOnDays.add((NumberOfTransactionsOnDay) o);
                        } else if (o instanceof NumberOfPersonsByAgeRange) {
                            numberOfPersonsByAgeRanges.add((NumberOfPersonsByAgeRange) o);
                        } else if (o instanceof NumberOfPersonsWithSpecificGender) {
                            numberOfPersonsWithSpecificGenderList.add((NumberOfPersonsWithSpecificGender) o);
                        }
                    }

                    setTransactionsData(numberOfTransactionsOnDays);
                    setAgeRangeData(numberOfPersonsByAgeRanges);
                    setGenderData(numberOfPersonsWithSpecificGenderList);
                });
    }

    private void setTransactionsData(List<NumberOfTransactionsOnDay> dataList) {
        if(!dataList.isEmpty()) {
            ArrayList<BarEntry> values = new ArrayList<>();
            final ArrayList<String> xVals = new ArrayList<>();
            final HashMap<String, Integer> transactionsByDay = new HashMap<>();

            for (int y = 0; y < 7; y++) {

                Date date = new Date();
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                cal.add(Calendar.DATE, -y);
                transactionsByDay.put(cal.get(Calendar.DAY_OF_MONTH) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR), 0);
            }

            for (NumberOfTransactionsOnDay data : dataList) {
                try {
                    Date date = new Date(data.getDay());
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    transactionsByDay.put(cal.get(Calendar.DAY_OF_MONTH) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR), data.getNumberOfTransactions());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            int i = 0;
            xVals.clear();

            for (Map.Entry<String, Integer> entry : entriesSortedByKeys(transactionsByDay)) {
                values.add(new BarEntry(i++, entry.getValue()));
                xVals.add(i - 1, entry.getKey().substring(0, entry.getKey().length() - 5));

            }

            BarDataSet set1;

            set1 = new BarDataSet(values, "The year 2017");

            set1.setDrawIcons(false);

            int startColor1 = ContextCompat.getColor(getActivity(), R.color.backgroundLastColor);
            int endColor1 = ContextCompat.getColor(getActivity(), R.color.backgroundFirstColor);

            List<GradientColor> gradientColors = new ArrayList<>();
            gradientColors.add(new GradientColor(startColor1, endColor1));
            set1.setGradientColors(gradientColors);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setBarWidth(0.9f);
            data.setDrawValues(false);
            binding.transactionsChart.setData(data);

            Legend l = binding.transactionsChart.getLegend();
            l.setEnabled(false);

            binding.transactionsChart.getAxisLeft().setDrawLabels(false);
            binding.transactionsChart.getAxisRight().setDrawLabels(false);
            binding.transactionsChart.getAxisLeft().setDrawGridLines(false);
            binding.transactionsChart.getXAxis().setDrawGridLines(false);
            binding.transactionsChart.getAxisRight().setDrawGridLines(false);

            XAxis xAxis = binding.transactionsChart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

            YAxis yAxis = binding.transactionsChart.getAxisLeft();
            yAxis.setEnabled(false);
            yAxis.setAxisMinimum(0f);

            YAxis yAxis2 = binding.transactionsChart.getAxisRight();
            yAxis2.setEnabled(false);

            binding.transactionsChart.getDescription().setEnabled(false);
            binding.transactionsChart.setTouchEnabled(true);

            br.com.monitoratec.gryncashandroid.util.MyMarkerView mv = new br.com.monitoratec.gryncashandroid.util.MyMarkerView(getActivity(), R.layout.item_marker, xVals);
            binding.transactionsChart.setMarker(mv);

            xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
//        xAxis.setGranularity(1f);
            xAxis.removeAllLimitLines();

            xAxis.setLabelCount(7);
            binding.transactionsChart.invalidate();
        }
    }

    private void setAgeRangeData(List<NumberOfPersonsByAgeRange> dataList) {
        if(!dataList.isEmpty()) {
            ArrayList<BarEntry> values = new ArrayList<>();
            final ArrayList<String> xVals = new ArrayList<>();
            int i = 0;

            for (NumberOfPersonsByAgeRange data : dataList) {
                values.add(new BarEntry(i++, data.getNumberOfPersons()));
                xVals.add(String.valueOf(data.getAgeRange()));
            }

            BarDataSet set1;

            set1 = new BarDataSet(values, "The year 2017");

            set1.setDrawIcons(false);

            int startColor1 = ContextCompat.getColor(getActivity(), R.color.backgroundLastColor);
            int endColor1 = ContextCompat.getColor(getActivity(), R.color.backgroundFirstColor);

            List<GradientColor> gradientColors = new ArrayList<>();
            gradientColors.add(new GradientColor(startColor1, endColor1));
            set1.setGradientColors(gradientColors);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
//        data.setValueTextSize(10f);
            data.setBarWidth(0.9f);
            data.setDrawValues(false);
            binding.ageChart.setData(data);

            Legend l = binding.ageChart.getLegend();
            l.setEnabled(false);

            binding.ageChart.getAxisLeft().setDrawLabels(false);
            binding.ageChart.getAxisRight().setDrawLabels(false);
            binding.ageChart.getAxisLeft().setDrawGridLines(false);
            binding.ageChart.getXAxis().setDrawGridLines(false);
            binding.ageChart.getAxisRight().setDrawGridLines(false);
//        binding.ageChart.setDrawValueAboveBar(false);

            XAxis xAxis = binding.ageChart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

            YAxis yAxis = binding.ageChart.getAxisLeft();
            yAxis.setEnabled(false);
            yAxis.setAxisMinimum(0f);

            YAxis yAxis2 = binding.ageChart.getAxisRight();
            yAxis2.setEnabled(false);

            binding.ageChart.getDescription().setEnabled(false);
            binding.ageChart.setTouchEnabled(true);

            br.com.monitoratec.gryncashandroid.util.MyMarkerView mv = new br.com.monitoratec.gryncashandroid.util.MyMarkerView(getActivity(), R.layout.item_marker, null);
            binding.ageChart.setMarker(mv);

            xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
            xAxis.setGranularity(1f);
            xAxis.removeAllLimitLines();
//        xAxis.setDrawAxisLine(false);

            xAxis.setLabelCount(dataList.size());
            binding.ageChart.invalidate();
        }
    }

    private void setGenderData(List<NumberOfPersonsWithSpecificGender> dataList) {
        if(!dataList.isEmpty()) {
            ArrayList<PieEntry> pieData = new ArrayList<>();
            ArrayList<Integer> colors = new ArrayList<>();

            int totalCounter;
            int maleCounter = 0;
            int femaleCounter = 0;

            for (NumberOfPersonsWithSpecificGender data : dataList) {
                try {
                    switch (data.getGender()) {
                        case Constants.MASCULINO:
                            maleCounter += data.getNumberOfPersons();
                            break;
                        case Constants.FEMININO:
                            femaleCounter += data.getNumberOfPersons();
                            break;
                    }
                } catch (Exception e) {

                }
            }

            pieData.add(new PieEntry(maleCounter, getResources().getColor(R.color.backgroundFirstColor)));
            colors.add(getResources().getColor(R.color.backgroundFirstColor));

            pieData.add(new PieEntry(femaleCounter, getResources().getColor(R.color.backgroundLastColor)));
            colors.add(getResources().getColor(R.color.backgroundLastColor));

            PieDataSet dataSet = new PieDataSet(pieData, "Gender Results");
            dataSet.setColors(colors);
            dataSet.setDrawValues(false);

            PieData data = new PieData(dataSet);
            binding.genderChart.setData(data);

            binding.genderChart.setDrawHoleEnabled(false);
            binding.genderChart.getDescription().setEnabled(false);

            Legend l = binding.genderChart.getLegend();
            l.setEnabled(false);

            totalCounter = maleCounter + femaleCounter;
            if(totalCounter != 0) {
                binding.genderChartSubtitleValue2.setText(String.format("%s%%", String.valueOf(maleCounter * 100 / totalCounter)));
                binding.genderChartSubtitleValue3.setText(String.format("%s%%", String.valueOf(femaleCounter * 100 / totalCounter)));
            } else {
                binding.genderChartSubtitleValue2.setText(String.format("%s%%", String.valueOf(0)));
                binding.genderChartSubtitleValue3.setText(String.format("%s%%", String.valueOf(0)));
            }
            binding.genderChart.invalidate();
        }
    }

    SortedSet<Map.Entry<String, Integer>> entriesSortedByKeys(Map<String, Integer> map) {
        SortedSet<Map.Entry<String, Integer>> sortedEntries = new TreeSet<>(
                (e1, e2) -> {
                    String[] day1Splited = e1.getKey().split("/");
                    String[] day2Splited = e2.getKey().split("/");

                    if (Integer.parseInt(day1Splited[2]) > Integer.parseInt(day2Splited[2])) {
                        return 1;
                    } else if (Integer.parseInt(day1Splited[2]) < Integer.parseInt(day2Splited[2])) {
                        return -1;
                    } else {
                        if (Integer.parseInt(day1Splited[1]) > Integer.parseInt(day2Splited[1])) {
                            return 1;
                        } else if (Integer.parseInt(day1Splited[1]) < Integer.parseInt(day2Splited[1])) {
                            return -1;
                        } else {
                            if (Integer.parseInt(day1Splited[0]) > Integer.parseInt(day2Splited[0])) {
                                return 1;
                            } else {
                                return -1;
                            }
                        }
                    }
                }
        );
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }

}
