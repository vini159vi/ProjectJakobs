package com.example.projectjakobs.ui.fragment;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.ChangePasswordFragmentBinding;
import com.example.projectjakobs.util.Constants;
import com.example.projectjakobs.util.SharedPreferencesUtil;
import com.example.projectjakobs.viewModel.ChangePasswordViewModel;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import static com.example.projectjakobs.util.Constants.*;
import static com.example.projectjakobs.util.Constants.Status.*;

public class ChangePasswordFragment extends Fragment {

    private ChangePasswordViewModel mViewModel;
    private ChangePasswordFragmentBinding binding;
    private String type;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.change_password_fragment, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ChangePasswordViewModel.class);
        binding.setViewModel(mViewModel);

        Bundle bundle = getArguments();
        if(bundle != null) {
            type = bundle.getString("TYPE");
        }

        init();
        setObservables();
        setOnClicks();
    }

    private void setOnClicks() {
        binding.btnBack.setOnClickListener(v -> Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigateUp());
    }

    private void init() {
        if(type.equals("USER")) {
            mViewModel.init(SharedPreferencesUtil.getUser(getContext()).getCpf());
        } else if (type.equals("SALON")) {
            mViewModel.init(SharedPreferencesUtil.getSalon(getContext()).getCnpj());
        }
    }

    private void setObservables(){
        mViewModel.response.observe(this, response -> {
            switch (response.status){
                case LOADING:

                    break;
                case ERROR:
                    Snackbar.make(getView(), response.error.getMessage(), Snackbar.LENGTH_SHORT).show();
                    break;
                case SUCCESS:
                    Snackbar.make(getView(), "Senha trocada com sucesso", Snackbar.LENGTH_SHORT).show();
                    Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigateUp();
                    break;
            }
        });
    }


}
