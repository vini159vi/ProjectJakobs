package com.example.projectjakobs.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.ActivityLoginBinding;
import com.example.projectjakobs.domain.Login;
import com.example.projectjakobs.impl.LoginActivityImpl;
import com.example.projectjakobs.util.SharedPreferencesUtil;

import io.reactivex.disposables.CompositeDisposable;

import static com.example.projectjakobs.util.Constants.CPF;


public class LoginActivity extends AppCompatActivity {

    ActivityLoginBinding binding;
    LoginActivityImpl mPresenter = new LoginActivityImpl(this);
    private Activity context = this;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        Glide.with(this)
                .load(R.drawable.img_background2)
                .centerCrop()
                .into(binding.ivBackground);

        binding.btnLogin.setOnClickListener(v -> compositeDisposable.add(mPresenter.login(getLoginFromView())
                .subscribe(login -> {
                    if (login.getUserType().equals(CPF)) {
                        compositeDisposable.add(mPresenter.getUser(login.getLogin())
                                .subscribe(user -> {
                                    Intent intent = new Intent(this, MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(intent);
                                    SharedPreferencesUtil.setUser(user, this);
                                    context.finish();

                                }, Throwable::printStackTrace));
                    } else {
                        compositeDisposable.add(mPresenter.getSalon(login.getLogin())
                                .subscribe(salon -> {
                                    Intent intent = new Intent(this, SalonMainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(intent);
                                    SharedPreferencesUtil.setSalon(salon, this);
                                    context.finish();

                                }, Throwable::printStackTrace));
                    }
                }, throwable -> Toast.makeText(this, "Usuario não encontrado", Toast.LENGTH_SHORT).show())));

    }

    private Login getLoginFromView() {
        Login login = new Login();
        login.setLogin(binding.tietUsername.getText().toString());
        login.setPassword(binding.tietPassword.getText().toString());
        return login;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }
}
