package com.example.projectjakobs.ui.fragment;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.FragmentHomeBinding;
import com.example.projectjakobs.domain.Salon;
import com.example.projectjakobs.impl.HomeFragmentImpl;
import com.example.projectjakobs.ui.adapter.SalonAdapter;
import com.example.projectjakobs.util.Constants;

import java.util.List;

import androidx.navigation.Navigation;

public class HomeFragment extends Fragment implements SalonAdapter.OnClick {

    FragmentHomeBinding binding;
    private OnFragmentInteractionListener mListener;
    private HomeFragmentImpl mPresenter = new HomeFragmentImpl(getActivity());
    private SalonAdapter adapter;

    public HomeFragment() {
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        mPresenter.getSalons()
                .subscribe(salonList -> configRecycler(salonList));
    }

    private void configRecycler(List<Salon> salonList) {
        adapter = new SalonAdapter(salonList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSalonClicked(Salon salon) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.SALON, salon);

        Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.action_navigation_home_to_salonDetailsFragment,
                bundle,
                null, null);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
