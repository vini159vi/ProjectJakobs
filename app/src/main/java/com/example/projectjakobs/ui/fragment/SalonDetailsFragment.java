package com.example.projectjakobs.ui.fragment;


import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.FragmentSalonDetailsBinding;
import com.example.projectjakobs.domain.Salon;
import com.example.projectjakobs.util.Constants;
import com.example.projectjakobs.util.SharedPreferencesUtil;

import androidx.navigation.Navigation;

/**
 * A simple {@link Fragment} subclass.
 */
public class SalonDetailsFragment extends Fragment {

    private FragmentSalonDetailsBinding binding;
    private Salon salon;

    public SalonDetailsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_salon_details, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        if (bundle != null) {
            salon = bundle.getParcelable(Constants.SALON);
            if(salon != null){
                populateView(salon);
            }
        } else {
            salon = SharedPreferencesUtil.getSalon(view.getContext());
            binding.btnSendSchedule.setVisibility(View.GONE);
            populateView(salon);
        }

        binding.btnSendSchedule.setOnClickListener(v -> {
            Bundle bundleSalon = new Bundle();
            bundleSalon.putParcelable("Salon", salon);

            Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.action_salonDetailsFragment_to_scheduleFragment,
                    bundleSalon,
                    null,
                    null);
        });
    }

    private void populateView(Salon salon) {
        binding.tvName.setText(salon.getName());
        binding.tvDescription.setText(salon.getDescription());
        binding.tvLocation.setText(salon.getAddressString());

        if(salon.getPhotoUrl() != null) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(8));

            Glide.with(this)
                    .load(salon.getPhotoUrl())
                    .fitCenter()
                    .apply(requestOptions)
                    .into(binding.ivSalonImage);
        } else {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(8));

            Glide.with(this)
                    .load(R.drawable.salon)
                    .fitCenter()
                    .apply(requestOptions)
                    .into(binding.ivSalonImage);
        }
    }
}
