package com.example.projectjakobs.ui.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.ActivityRegisterPeopleBinding;
import com.example.projectjakobs.domain.User;
import com.example.projectjakobs.impl.RegisterPeopleImpl;
import com.example.projectjakobs.util.Constants;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import id.zelory.compressor.Compressor;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.aprilapps.easyphotopicker.EasyImageConfig;

import static com.example.projectjakobs.util.Constants.USER;

public class RegisterPeopleActivity extends AppCompatActivity {

    private RegisterPeopleImpl mPresenter = new RegisterPeopleImpl(this);
    private ActivityRegisterPeopleBinding binding;
    private User user = new User();
    private Context context = this;
    private File userImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register_people);

        setOnClicks();

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            user = bundle.getParcelable(USER);
            user.setGender(Constants.MASCULINO);
        }
    }

    private void setOnClicks() {
        Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel(myCalendar);
        };

        binding.fabEdit.setOnClickListener(v -> {
            EasyImage.openCamera(this, EasyImageConfig.REQ_TAKE_PICTURE);
        });

        binding.tietDateOfBirth.setOnClickListener(v -> new DatePickerDialog(this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());

        binding.btnCreateAccount.setOnClickListener(v -> {
            if (isValid()) {
                populateUserFromView();

                mPresenter.registerUser(user, userImage)
                        .subscribe(() -> {
                            Intent intent = new Intent(this, MainActivity.class);
                            startActivity(intent);
                        });
            }
        });
    }

    private boolean isValid() {
        boolean isValid = true;

        if (binding.tietName.getText() == null && binding.tietName.getText().toString().isEmpty()) {
            isValid = false;
        }

        if (binding.tietCpf.getText() == null && binding.tietCpf.getText().toString().isEmpty()) {
            isValid = false;
        }

        if (binding.tietCellphoneNumber.getText() == null && binding.tietCellphoneNumber.getText().toString().isEmpty()) {
            isValid = false;
        }

        if (binding.tietDateOfBirth.getText() == null && binding.tietDateOfBirth.getText().toString().isEmpty()) {
            isValid = false;
        }

        return isValid;
    }

    private void populateUserFromView() {
        if (binding.tietName.getText() != null && !binding.tietName.getText().toString().isEmpty()) {
            user.setName(binding.tietName.getText().toString());
        }
        if (binding.tietCellphoneNumber.getText() != null && !binding.tietCellphoneNumber.getText().toString().isEmpty()) {
            user.setPhone(binding.tietCellphoneNumber.getText().toString());
        }
        if (binding.tietCpf.getText() != null && !binding.tietCpf.getText().toString().isEmpty()) {
            user.setCpf(binding.tietCpf.getText().toString());
        }

        if (binding.tietDateOfBirth.getText() != null && !binding.tietDateOfBirth.getText().toString().isEmpty()) {
            user.setDateOfBirth(binding.tietDateOfBirth.getText().toString());
        }
    }

    private void updateLabel(Calendar myCalendar) {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, new Locale("pt", "BR"));

        binding.tietDateOfBirth.setText(sdf.format(myCalendar.getTime()));
    }

    public void onRadioButtonClicked(View view) {

        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.radio_male:
                if (checked)
                    user.setGender(Constants.MASCULINO);
                    break;
            case R.id.radio_female:
                if (checked)
                    user.setGender(Constants.FEMININO);
                    break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                try {
                    userImage = new Compressor(context)
                            .setQuality(75)
                            .compressToFile(new File(imageFile.getPath()));

                    Glide.with(context)
                            .load(userImage)
                            .into(binding.ivLogo);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                binding.ivCamera.setVisibility(View.GONE);
            }
        });
    }
}
