package com.example.projectjakobs.ui.activities;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projectjakobs.impl.SplashActivityImpl;
import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.ActivitySplashBinding;

public class SplashActivity extends AppCompatActivity {

    ActivitySplashBinding binding;
    SplashActivityImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        mPresenter = new SplashActivityImpl();

        Handler handler = new Handler();
        handler.postDelayed(() -> {
            Intent intent = new Intent(this, SelectRoleLoginActivity.class);
            startActivity(intent);
            finish();
        }, 1500);
    }
}
