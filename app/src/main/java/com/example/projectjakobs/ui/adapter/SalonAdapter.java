package com.example.projectjakobs.ui.adapter;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.projectjakobs.domain.Salon;
import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.ItemHairSalonBinding;

import java.util.List;

public class SalonAdapter extends RecyclerView.Adapter<SalonAdapter.ViewHolder> {

    List<Salon> salonList;
    OnClick onClick;

    public SalonAdapter(List<Salon> salonList, OnClick onClick) {
        this.salonList = salonList;
        this.onClick = onClick;
    }

    @NonNull
    @Override
    public SalonAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        ItemHairSalonBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_hair_salon, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SalonAdapter.ViewHolder viewHolder, int i) {
        Salon salon = salonList.get(i);
        viewHolder.binding.name.setText(salon.getName());
        viewHolder.binding.description.setText(salon.getDescription());
        viewHolder.binding.location.setText(salon.getAddressString());

        if(salon.getPhotoUrl() != null) {
            Glide.with(viewHolder.itemView)
                    .load(salon.getPhotoUrl())
                    .fitCenter()
                    .into(viewHolder.binding.ivStore);
        }

        viewHolder.binding.salonLayout.setOnClickListener(v -> onClick.onSalonClicked(salon));
    }

    @Override
    public int getItemCount() {
        return salonList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ItemHairSalonBinding binding;

        public ViewHolder(ItemHairSalonBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface OnClick {
        void onSalonClicked(Salon salon);
    }
}
