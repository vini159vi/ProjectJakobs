package com.example.projectjakobs.ui.activities;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.ActivityMainBinding;

import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        NavigationUI.setupWithNavController(binding.navigation, Navigation.findNavController(this, R.id.nav_host_fragment));
    }

}
