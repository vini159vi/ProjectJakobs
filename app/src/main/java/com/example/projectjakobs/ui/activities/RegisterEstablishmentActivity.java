package com.example.projectjakobs.ui.activities;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.ActivityRegisterEstablishmentBinding;
import com.example.projectjakobs.domain.Salon;
import com.example.projectjakobs.impl.RegisterEstablishmentImpl;

import java.io.File;
import java.io.IOException;

import id.zelory.compressor.Compressor;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.aprilapps.easyphotopicker.EasyImageConfig;

public class RegisterEstablishmentActivity extends AppCompatActivity {
    private RegisterEstablishmentImpl impl = new RegisterEstablishmentImpl(this);
    private ActivityRegisterEstablishmentBinding binding;
    private File storeImage;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register_establishment);

        setOnclicks();
    }

    private void setOnclicks() {
        binding.btnRegister.setOnClickListener(v -> {
            if(isValid()) {
                impl.setNewSalon(getSalonFromView(), binding.tietPassword.getText().toString(), storeImage)
                        .subscribe(() -> {
                            Intent intent = new Intent(this, SalonMainActivity.class);
                            startActivity(intent);
                            finish();
                        });
            } else {
                Toast.makeText(this, "Faltando campos para preencher", Toast.LENGTH_SHORT).show();
            }
        });

        binding.fabEdit.setOnClickListener(v -> {
            EasyImage.openCamera(this, EasyImageConfig.REQ_TAKE_PICTURE);
        });
    }

    private boolean isValid() {
        boolean isValid = true;
        if(binding.tietName.getText().toString().isEmpty()) {
            isValid = false;
        }
        if(binding.tietAddress.getText().toString().isEmpty()) {
            isValid = false;
        }
        if(binding.tietCnpj.getText().toString().isEmpty()) {
            isValid = false;
        }
        if(binding.tietCellphoneNumber.getText().toString().isEmpty()) {
            isValid = false;
        }
        if(binding.tietPassword.getText().toString().isEmpty()) {
            isValid = false;
        }
        return isValid;
    }

    private Salon getSalonFromView(){
        Salon salon = new Salon();
        salon.setName(binding.tietName.getText().toString());
        salon.setAddressString(binding.tietAddress.getText().toString());
        salon.setCnpj(binding.tietCnpj.getText().toString());
        salon.setPhone(binding.tietCellphoneNumber.getText().toString());
        salon.setDescription(binding.tietDescription.getText().toString());
        return salon;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                try {
                    storeImage = new Compressor(context)
                            .setQuality(75)
                            .compressToFile(new File(imageFile.getPath()));

                    Glide.with(context)
                            .load(storeImage)
                            .into(binding.ivLogo);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                binding.ivCamera.setVisibility(View.GONE);
            }
        });
    }
}
