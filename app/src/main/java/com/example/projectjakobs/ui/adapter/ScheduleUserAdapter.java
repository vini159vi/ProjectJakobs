package com.example.projectjakobs.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.ItemAppointmentBinding;
import com.example.projectjakobs.domain.Schedule;
import com.example.projectjakobs.repository.ScheduleRepository;
import com.example.projectjakobs.ui.activities.SalonMainActivity;
import com.example.projectjakobs.ui.fragment.ScheduleUserFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ScheduleUserAdapter extends RecyclerView.Adapter<ScheduleUserAdapter.ViewHolder> {

    private List<Schedule> schedules;
    private ScheduleRepository scheduleRepository = new ScheduleRepository();
    private Fragment context;

    public ScheduleUserAdapter(Fragment context, List<Schedule> schedules) {
        this.context = context;

        if (schedules != null) {
            Collections.reverse(schedules);
            this.schedules = schedules;
        } else {
            this.schedules = new ArrayList<>();
        }
    }

    @NonNull
    @Override
    public ScheduleUserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        ItemAppointmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_appointment, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ScheduleUserAdapter.ViewHolder viewHolder, int i) {
        Schedule schedule = schedules.get(i);
        Resources res = viewHolder.itemView.getResources();
        viewHolder.binding.tvName.setText(schedule.getNameSalon());

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy");
        viewHolder.binding.tvDate.setText("Data " + format.format(new Date(schedule.getDateMarked())));

        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
        viewHolder.binding.tvHour.setText("Agendado ás " + formatTime.format(new Date(schedule.getDateMarked())));

        if (schedule.isAccepted() != null && schedule.isAccepted()) {
            viewHolder.binding.tvStatus.setText("Aceitado");
            viewHolder.binding.tvStatus.setTextColor(res.getColor(R.color.green));
        } else if (schedule.isAccepted() != null && !schedule.isAccepted()) {
            viewHolder.binding.tvStatus.setText("Recusado");
            viewHolder.binding.tvStatus.setTextColor(res.getColor(R.color.red));
        } else {
            viewHolder.binding.tvStatus.setText("Aguardando...");
            viewHolder.binding.tvStatus.setTextColor(res.getColor(R.color.secondary_text));
        }

        if (schedule.getSalonPhone() != null && !schedule.getSalonPhone().isEmpty()) {
            viewHolder.binding.btnPhone.setVisibility(View.VISIBLE);
            viewHolder.binding.btnPhone.setOnClickListener(v -> {
                ((ScheduleUserFragment) context).openDialer(schedule.getSalonPhone());
            });
        } else {
            viewHolder.binding.btnPhone.setVisibility(View.GONE);
        }


        if (schedule.getSalonPhotoUrl() != null && !schedule.getSalonPhotoUrl().isEmpty()) {
            Glide.with(viewHolder.itemView)
                    .load(schedule.getSalonPhotoUrl())
                    .into(viewHolder.binding.civPhoto);
        }
    }

    private void deleteComponent(Schedule schedule) {
        schedules.remove(schedule);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return schedules.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ItemAppointmentBinding binding;

        public ViewHolder(ItemAppointmentBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
