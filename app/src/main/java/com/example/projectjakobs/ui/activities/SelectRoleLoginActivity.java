package com.example.projectjakobs.ui.activities;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.ActivitySelectRoleLoginBinding;

public class SelectRoleLoginActivity extends AppCompatActivity {

    ActivitySelectRoleLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_role_login);

        binding.btnEnter.setOnClickListener(v -> {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        });

        binding.btnRegisterLikeUser.setOnClickListener(v -> {
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivity(intent);
        });

        binding.btnRegisterEstablishment.setOnClickListener(v -> {
            Intent intent = new Intent(this, RegisterEstablishmentActivity.class);
            startActivity(intent);
        });

        Glide.with(this)
                .load(R.drawable.img_background3)
                .centerCrop()
                .into(binding.ivBackground);
    }
}
