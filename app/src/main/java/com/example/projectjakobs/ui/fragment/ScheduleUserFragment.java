package com.example.projectjakobs.ui.fragment;


import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.FragmentScheduleUserBinding;
import com.example.projectjakobs.domain.Schedule;
import com.example.projectjakobs.impl.ScheduleUserImpl;
import com.example.projectjakobs.ui.adapter.ScheduleUserAdapter;
import com.example.projectjakobs.util.SharedPreferencesUtil;

import java.util.List;

public class ScheduleUserFragment extends Fragment {

    private FragmentScheduleUserBinding binding;
    private ScheduleUserAdapter adapter;
    private ScheduleUserImpl mImpl = new ScheduleUserImpl();

    public ScheduleUserFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_schedule_user, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mImpl.getSchedules(SharedPreferencesUtil.getUser(getContext()).getCpf())
                .subscribe((schedules, throwable) -> configRecycler(schedules));
    }

    private void configRecycler(List<Schedule> schedules) {
        if(schedules.isEmpty()) {
            binding.tvEmpty.setVisibility(View.VISIBLE);
        }
        adapter = new ScheduleUserAdapter(this, schedules);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.rvSchedulers.setLayoutManager(layoutManager);
        binding.rvSchedulers.setAdapter(adapter);
    }

    public void openDialer(String cellphone) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + cellphone));
        getActivity().startActivity(intent);
    }
}
