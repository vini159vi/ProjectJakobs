package com.example.projectjakobs.ui.fragment;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.FragmentNewScheduleBinding;
import com.example.projectjakobs.domain.Salon;
import com.example.projectjakobs.domain.Schedule;
import com.example.projectjakobs.repository.ScheduleRepository;
import com.example.projectjakobs.util.SharedPreferencesUtil;
import io.reactivex.schedulers.Schedulers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class NewScheduleFragment extends Fragment {

    FragmentNewScheduleBinding binding;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Calendar dateFinal = Calendar.getInstance();
    private Salon salon = new Salon();

    public NewScheduleFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_schedule, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            salon = bundle.getParcelable("Salon");
        }

        setOnClicks();
    }

    private void setOnClicks() {
        binding.tilDate.setOnClickListener(v -> {

        });

        binding.tilTime.setOnClickListener(v -> {

        });

        binding.tietDate.setOnClickListener(v -> {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view1, year, month, dayOfMonth) -> {
                binding.tietDate.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                dateFinal.set(Calendar.YEAR, year);
                dateFinal.set(Calendar.MONTH, month);
                dateFinal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            datePickerDialog.show();
        });
        binding.tietTime.setOnClickListener(v -> {
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), (view12, hourOfDay, minute) -> {
                binding.tietTime.setText(hourOfDay + ":" + minute);
                dateFinal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                dateFinal.set(Calendar.MINUTE, minute);
            }, mHour, mMinute, true);
            timePickerDialog.show();
        });

        binding.btnSendSchedule.setOnClickListener(v -> {
            if (!binding.tietTime.getText().toString().trim().isEmpty() && !binding.tietDate.getText().toString().trim().isEmpty()) {
                Schedule schedule = new Schedule();
                schedule.setDateIssue(System.currentTimeMillis());
                schedule.setDateMarked(dateFinal.getTime().getTime());
                schedule.setDeleted(false);
                schedule.setCpfClient(Long.valueOf(SharedPreferencesUtil.getUser(getContext()).getCpf()));
                schedule.setNameClient(SharedPreferencesUtil.getUser(getContext()).getName());
                schedule.setNameSalon(salon.getName());
                schedule.setCnpjSalon(Long.valueOf(salon.getCnpj()));
                schedule.setGenderClient(SharedPreferencesUtil.getUser(getContext()).getGender());
                schedule.setSalonPhotoUrl(salon.getPhotoUrl());
                schedule.setSalonPhone(salon.getPhone());

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date date = sdf.parse(SharedPreferencesUtil.getUser(getContext()).getDateOfBirth());
                    Date today = new Date(System.currentTimeMillis());
                    int age =  today.getYear() - date.getYear();
                    schedule.setAgeClient(age);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                ScheduleRepository repository = new ScheduleRepository();
                repository.setNewSchedule(schedule, salon)
                        .subscribeOn(Schedulers.computation())
                        .subscribe(() -> Snackbar.make(getView(), "Tudo certo, seu horario foi enviado para o responsável pelo estabelecimento", Snackbar.LENGTH_SHORT).show());
            } else {
                Snackbar.make(getView(), "Algum dos campos está faltando", Snackbar.LENGTH_SHORT).show();
            }
        });
    }
}
