package com.example.projectjakobs.ui.fragment;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.FragmentProfileBinding;
import com.example.projectjakobs.domain.Salon;
import com.example.projectjakobs.domain.User;
import com.example.projectjakobs.impl.ProfileFragmentImpl;
import com.example.projectjakobs.ui.activities.MainActivity;
import com.example.projectjakobs.ui.activities.SalonMainActivity;
import com.example.projectjakobs.ui.activities.SelectRoleLoginActivity;
import com.example.projectjakobs.util.MaskWatcher;
import com.example.projectjakobs.util.SharedPreferencesUtil;

public class ProfileFragment extends Fragment {

    private FragmentProfileBinding binding;
    private ProfileFragmentImpl mImpl;

    public ProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mImpl = new ProfileFragmentImpl(view.getContext());

        if(getActivity() instanceof MainActivity) {
            User user = SharedPreferencesUtil.getUser(view.getContext());
            if (user != null) {
                populateView(user);
            }
        } else if (getActivity() instanceof SalonMainActivity) {
            Salon salon = SharedPreferencesUtil.getSalon(view.getContext());
            if (salon != null) {
                populateView(salon);
            }
        }


        setOnClicks();
    }

    private void setOnClicks() {
        binding.clMyProfile.setOnClickListener(v -> {
            //TODO Tela de editar
        });

        binding.clChangePassword.setOnClickListener(v -> {
            if(getActivity() instanceof MainActivity) {
                Bundle bundle = new Bundle();
                bundle.putString("TYPE", "USER");
                Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.action_navigation_profile_to_changePasswordFragment, bundle);
            } else if (getActivity() instanceof  SalonMainActivity) {
                Bundle bundle = new Bundle();
                bundle.putString("TYPE", "SALON");
                Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.action_navigation_profile_to_changePasswordFragment2, bundle);
            }
        });

        binding.clExit.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), SelectRoleLoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            Toast.makeText(getContext(), "Você foi deslogado", Toast.LENGTH_SHORT).show();

            startActivity(intent);
            getActivity().finish();
        });
    }

    private void populateView(User user) {
        binding.name.setText(user.getName());
        binding.cpf.setText(String.format("CPF: %s", MaskWatcher.addMask(user.getCpf(), MaskWatcher.CPF_MASK)));
        if(user.getPhone().length() <= 10) {
            binding.phone.setText(String.format("Telefone: %s", MaskWatcher.addMask(user.getPhone(), MaskWatcher.PHONE_MASK)));
        } else {
            binding.phone.setText(String.format("Celular: %s", MaskWatcher.addMask(user.getPhone(), MaskWatcher.SMARTPHONE_MASK)));
        }
        binding.tvEmail.setText(String.format("Email: %s", user.getEmail()));

        if(user.getPhotoUrl() != null && !user.getPhotoUrl().isEmpty()){
            binding.ivPerson.setVisibility(View.GONE);
            Glide.with(this)
                    .load(user.getPhotoUrl())
                    .fitCenter()
                    .into(binding.civPhoto);
        } else {
            binding.ivPerson.setVisibility(View.VISIBLE);
        }
    }

    private void populateView(Salon salon) {
        binding.name.setText(salon.getName());
        binding.cpf.setText(String.format("CNPJ: %s", MaskWatcher.addMask(salon.getCnpj(), MaskWatcher.CNPJ_MASK)));
        if(salon.getPhone().length() <= 10) {
            binding.phone.setText(String.format("Telefone: %s", MaskWatcher.addMask(salon.getPhone(), MaskWatcher.PHONE_MASK)));
        } else {
            binding.phone.setText(String.format("Celular: %s", MaskWatcher.addMask(salon.getPhone(), MaskWatcher.SMARTPHONE_MASK)));
        }
        binding.tvEmail.setVisibility(View.GONE);

        if(salon.getPhotoUrl() != null && !salon.getPhotoUrl().isEmpty()){
            binding.ivPerson.setVisibility(View.GONE);
            Glide.with(this)
                    .load(salon.getPhotoUrl())
                    .fitCenter()
                    .into(binding.civPhoto);
        } else {
            binding.ivPerson.setVisibility(View.VISIBLE);
        }
    }
}
