package com.example.projectjakobs.ui.adapter;

import android.content.res.Resources;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.ItemScheduleBinding;
import com.example.projectjakobs.domain.Schedule;
import com.example.projectjakobs.repository.ScheduleRepository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.reactivex.schedulers.Schedulers;

public class ScheduleSalonAdapter extends RecyclerView.Adapter<ScheduleSalonAdapter.ViewHolder> {

    private List<Schedule> schedules;
    private ScheduleRepository scheduleRepository = new ScheduleRepository();

    public ScheduleSalonAdapter(List<Schedule> schedules) {
        if (schedules != null) {
            Collections.reverse(schedules);
            this.schedules = schedules;
        } else {
            this.schedules = new ArrayList<>();
        }
    }

    @NonNull
    @Override
    public ScheduleSalonAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        ItemScheduleBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_schedule, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ScheduleSalonAdapter.ViewHolder viewHolder, int i) {
        Schedule schedule = schedules.get(i);
        Resources res = viewHolder.itemView.getResources();
        viewHolder.binding.tvName.setText(schedule.getNameClient());

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        viewHolder.binding.tvDate.setText(format.format(new Date(schedule.getDateMarked())));

        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
        viewHolder.binding.tvTime.setText(formatTime.format(new Date(schedule.getDateMarked())));

        if(schedule.isAccepted() == null) {
            viewHolder.binding.ivAccept.setOnClickListener(v -> {
                scheduleRepository.acceptSchedule(schedule)
                        .subscribeOn(Schedulers.computation())
                        .subscribe(() -> {
                            Snackbar.make(viewHolder.itemView, "Agendamento aceito", Snackbar.LENGTH_SHORT).show();
                            schedule.setAccepted(true);
                            notifyItemChanged(i);
                        });
            });

            viewHolder.binding.ivCancel.setOnClickListener(v -> {
                scheduleRepository.refuseSchedule(schedule)
                        .subscribeOn(Schedulers.computation())
                        .subscribe(() -> {
                            Snackbar.make(viewHolder.itemView, "Agendamento rejeitado", Snackbar.LENGTH_SHORT).show();
                            schedule.setAccepted(false);
                            notifyItemChanged(i);
                        });
            });
        } else {
            if(schedule.isAccepted() != null && schedule.isAccepted()) {
                viewHolder.binding.tvStatus.setText("Aceitado");
                viewHolder.binding.tvStatus.setTextColor(res.getColor(R.color.green));
            } else if (schedule.isAccepted() != null && !schedule.isAccepted()){
                viewHolder.binding.tvStatus.setText("Recusado");
                viewHolder.binding.tvStatus.setTextColor(res.getColor(R.color.red));
            }
            viewHolder.binding.ivAccept.setVisibility(View.GONE);
            viewHolder.binding.ivCancel.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return schedules.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ItemScheduleBinding binding;

        public ViewHolder(ItemScheduleBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
