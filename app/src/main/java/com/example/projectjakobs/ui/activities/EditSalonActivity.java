package com.example.projectjakobs.ui.activities;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;
import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.ActivityEditSalonBinding;
import com.example.projectjakobs.domain.Salon;
import com.example.projectjakobs.repository.SalonRepository;
import io.reactivex.schedulers.Schedulers;

public class EditSalonActivity extends AppCompatActivity {

    ActivityEditSalonBinding binding;
    SalonRepository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_salon);
        getSalon();
    }

    private void getSalon() {
        repository = new SalonRepository();
        repository.getSalon(getIntent().getStringExtra("cnpj"))
                .subscribeOn(Schedulers.computation())
                .subscribe(salon -> populateView(salon));
    }

    private void populateView(Salon salon) {
        binding.tietName.setText(salon.getName());
        binding.tietAddress.setText(salon.getAddressString());
        binding.tietCellphoneNumber.setText(String.valueOf(salon.getPhone()));
        binding.tietCpf.setText(String.valueOf(salon.getCnpj()));
        binding.tilPassword.setVisibility(View.GONE);
        binding.tvLabelPersonData.setText("Coloque os novos dados de seu salão");
        binding.btnRegister.setText("Salvar alterações");
//        binding.btnRegister.setOnClickListener(v -> repository.setNewSalon(getSalonFromView())
//                .subscribe(this::finish));/
        binding.btnDelete.setOnClickListener(v -> repository.deleteSalon(getIntent().getStringExtra("cnpj"))
                .subscribe(() -> {
                    Intent intent = new Intent(this, SelectRoleLoginActivity.class);
                    startActivity(intent);
                    Toast.makeText(this, "Deletado com sucesso", Toast.LENGTH_SHORT).show();
                }));
    }

}
