package com.example.projectjakobs.ui.fragment;


import android.annotation.SuppressLint;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.FragmentScheduleSalonBinding;
import com.example.projectjakobs.domain.Schedule;
import com.example.projectjakobs.impl.ScheduleSalonImpl;
import com.example.projectjakobs.ui.adapter.ScheduleSalonAdapter;
import com.example.projectjakobs.util.SharedPreferencesUtil;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleSalonFragment extends Fragment {

    private FragmentScheduleSalonBinding binding;
    private ScheduleSalonAdapter adapter;
    private ScheduleSalonImpl mImpl = new ScheduleSalonImpl();

    public ScheduleSalonFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_schedule_salon, container, false);
        return binding.getRoot();
    }

    @SuppressLint("CheckResult")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        mImpl.getSchedules(SharedPreferencesUtil.getSalon(getContext()).getCnpj())
                .subscribe((schedules, throwable) -> {
                    configRecycler(schedules);
                });

    }

    private void configRecycler(List<Schedule> schedules) {
        if(schedules.isEmpty()) {
            binding.tvEmpty.setVisibility(View.VISIBLE);
        }
        adapter = new ScheduleSalonAdapter(schedules);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.rvSchedulersSalon.setLayoutManager(layoutManager);
        binding.rvSchedulersSalon.setAdapter(adapter);
    }
}
