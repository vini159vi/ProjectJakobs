package com.example.projectjakobs.ui.activities;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;

import com.example.projectjakobs.R;
import com.example.projectjakobs.databinding.ActivityRegisterBinding;
import com.example.projectjakobs.domain.User;
import com.example.projectjakobs.impl.RegisterActivityImpl;

public class RegisterActivity extends AppCompatActivity {

    ActivityRegisterBinding binding;
    RegisterActivityImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        mPresenter = new RegisterActivityImpl(this);

        configOnClicks();
    }


    private User getUserFromView(){
        User user = new User();

        if(binding.tietUsername.getText() != null && !binding.tietUsername.getText().toString().isEmpty()) {
            user.setUsername(binding.tietUsername.getText().toString());
        }

        if(binding.tietEmail.getText() != null && !binding.tietEmail.getText().toString().isEmpty()){
            user.setEmail(binding.tietEmail.getText().toString());
        }

        if(binding.tietPassword.getText() != null && !binding.tietPassword.getText().toString().isEmpty()){
            user.setPassword(binding.tietPassword.getText().toString());
        }

        return user;
    }


    private void configOnClicks() {
        binding.btnRegister.setOnClickListener(v -> {
            if(isValid()) {
                mPresenter.next(getUserFromView());
            } else {
                Toast.makeText(this, "Algum campo está errado", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean isValid() {
        boolean isValid = true;

        if(binding.tietUsername.getText() == null && binding.tietUsername.getText().toString().isEmpty()) {
            isValid = false;
        }

        if(binding.tietEmail.getText() == null && binding.tietEmail.getText().toString().isEmpty()){
            isValid = false;
        }

        if(binding.tietPassword.getText() == null && binding.tietPassword.getText().toString().isEmpty()){
            isValid = false;
        } else if (binding.tietPasswordAgain.getText() == null && binding.tietPasswordAgain.getText().toString().isEmpty()){
            isValid = false;
        } else if(!binding.tietPassword.getText().toString().equals(binding.tietPasswordAgain.getText().toString())){
            isValid = false;
        }

        return isValid;
    }

}
