package com.example.projectjakobs.repository;

import android.annotation.SuppressLint;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.example.projectjakobs.domain.Salon;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class SalonRepository {

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference reference = database.getReference("salon");
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageRef = storage.getReference();

    public Completable setNewSalon(Salon salon) {
        return Completable.create(emitter -> {
            reference.child(salon.getCnpj()).setValue(salon);
            emitter.onComplete();
        });
    }

    public Single<Salon> getSalon(String cnpj) {
        return Single.create(emitter -> {
            reference.child(cnpj).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null) {
                        Salon salon = dataSnapshot.getValue(Salon.class);
                        emitter.onSuccess(salon);
                    } else {
                        emitter.onError(new Throwable());
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        });
    }

    public Completable deleteSalon(String cnpj) {
        return Completable.create(emitter -> {
            reference.child(cnpj).removeValue();
            emitter.onComplete();
        });
    }

    public Single<List<Salon>> getSalons() {
        return Single.create(emitter -> reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Salon> salonList = new ArrayList<>();
                if (dataSnapshot != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        Salon salon = postSnapshot.getValue(Salon.class);
                        if (salon != null) {
                            salonList.add(salon);
                        }
                    }
                    emitter.onSuccess(salonList);
                } else {
                    emitter.onError(new Throwable());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        }));
    }

    @SuppressLint("CheckResult")
    public Completable setNewSalon(Salon salon, File file) {
        return Completable.create(emitter -> {
            sendImage(file, salon.getName())
                    .subscribeOn(Schedulers.io())
                    .subscribe((s, throwable) -> {
                        salon.setPhotoUrl(s);
                        reference.child(salon.getCnpj()).setValue(salon)
                                .addOnSuccessListener(task -> emitter.onComplete());
                    });
        });
    }

    public Single<String> sendImage(File file, String name) {
        return Single.create(emitter -> {
            Uri uri = Uri.fromFile(file);
            StorageReference imageRef = storageRef.child("images/" + name);
            UploadTask uploadTask = imageRef.putFile(uri);
            uploadTask
                    .addOnFailureListener(emitter::onError)
                    .addOnSuccessListener(taskSnapshot -> uploadTask.continueWithTask(task -> {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return imageRef.getDownloadUrl();
                    }).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            emitter.onSuccess(task.getResult().toString());
                        }
                    }));
        });
    }
}
