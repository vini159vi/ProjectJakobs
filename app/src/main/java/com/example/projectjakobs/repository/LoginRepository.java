package com.example.projectjakobs.repository;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.example.projectjakobs.domain.Login;
import com.example.projectjakobs.domain.Response;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

public class LoginRepository {
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference reference;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LoginRepository() {
        reference = database.getReference("login");
    }

    public Completable setNewAccount(Login login) {
        return Completable.create(emitter -> {
            reference.child(login.getLogin()).setValue(login);
            emitter.onComplete();
        });
    }

    public Single<Login> login(Login login) {
        return Single.create(emitter -> reference.child(login.getLogin()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    Login loginDB = dataSnapshot.getValue(Login.class);
                    if (loginDB != null && loginDB.getPassword().equals(login.getPassword())) {
                        emitter.onSuccess(loginDB);
                    } else {
                        emitter.onError(new Throwable());
                    }
                } else {
                    emitter.onError(new Throwable());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        }));
    }

    public Completable verifyPassword(String cpf, String oldPassword) {
        return Completable.create(emitter -> reference.child(cpf).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    Login loginDB = dataSnapshot.getValue(Login.class);
                    if (loginDB != null && loginDB.getPassword().equals(oldPassword)) {
                        emitter.onComplete();
                    } else {
                        emitter.onError(new Throwable("Senha antiga errada"));
                    }
                } else {
                    emitter.onError(new Throwable("Erro de servidor"));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        }));
    }

    public void changePassword(String cpf, String oldPassword, String newPassword, MutableLiveData<Response> response) {
        compositeDisposable.add(verifyPassword(cpf, oldPassword)
                .subscribe(() -> compositeDisposable.add(Completable.create(emitter -> {
                            reference.child(cpf).child("password").setValue(newPassword)
                                    .addOnSuccessListener(aVoid -> emitter.onComplete())
                                    .addOnFailureListener(emitter::onError);
                        }).subscribe(() -> response.setValue(Response.success(newPassword)),
                        throwable -> response.setValue(Response.error(new Throwable("Erro de servidor"))))
                ), throwable -> response.setValue(Response.error(throwable))
        ));
    }
}
