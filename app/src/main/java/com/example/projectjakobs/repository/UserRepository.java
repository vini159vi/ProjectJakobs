package com.example.projectjakobs.repository;

import android.annotation.SuppressLint;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.example.projectjakobs.domain.Schedule;
import com.example.projectjakobs.domain.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class UserRepository {
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageRef = storage.getReference();
    private DatabaseReference reference;

    public UserRepository() {
        reference = database.getReference("user");
    }

    public Completable registerUser(User user) {
        return Completable.create(emitter -> reference.child(user.getCpf()).setValue(user)
                .addOnSuccessListener(task -> emitter.onComplete()));
    }

    @SuppressLint("CheckResult")
    public Completable registerUser(User user, File file) {
        return Completable.create(emitter -> {
            sendImage(file, user.getUsername())
                    .subscribe((s, throwable) -> {
                        user.setPhotoUrl(s);
                        reference.child(user.getCpf()).setValue(user)
                                .addOnSuccessListener(task -> emitter.onComplete());
                    });
        });
    }

    public Single<User> getUser(String cpf) {
        return Single.create(emitter -> reference.child(cpf).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    emitter.onSuccess(user);
                } else {
                    emitter.onError(new Throwable());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        }));
    }

    public Single<List<Schedule>> getUserSchedules(String cpf) {
        return Single.create(emitter -> {
            reference.child(cpf).child("schedules").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null && dataSnapshot.getChildren() != null) {
                        List<Schedule> schedules = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Schedule schedule = snapshot.getValue(Schedule.class);
                            if (schedule != null && !schedule.isDeleted()) {
                                schedules.add(schedule);
                            }
                        }
                        emitter.onSuccess(schedules);
                    } else {
                        emitter.onError(new Throwable());
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        });
    }

    public Single<String> sendImage(File file, String userName) {
        return Single.create(emitter -> {
            Uri uri = Uri.fromFile(file);
            StorageReference imageRef = storageRef.child("images/" + userName);
            UploadTask uploadTask = imageRef.putFile(uri);
            uploadTask
                    .addOnFailureListener(emitter::onError)
                    .addOnSuccessListener(taskSnapshot -> uploadTask.continueWithTask(task -> {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return imageRef.getDownloadUrl();
                    }).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            emitter.onSuccess(task.getResult().toString());
                        }
                    }));
        });
    }
}
