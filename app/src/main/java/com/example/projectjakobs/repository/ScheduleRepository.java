package com.example.projectjakobs.repository;

import androidx.annotation.NonNull;

import com.example.projectjakobs.domain.Salon;
import com.example.projectjakobs.domain.Schedule;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class ScheduleRepository {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference reference;

    public ScheduleRepository() {
        reference = database.getReference("schedule");
    }

    public Completable setNewSchedule(Schedule schedule, Salon salon) {
        return Completable.create(emitter -> {
            String id = String.valueOf(schedule.getDateIssue()) + String.valueOf(schedule.getCpfClient());
            schedule.setId(id);
            reference.child(salon.getCnpj()).child(id).setValue(schedule);
            database.getReference("user").child(String.valueOf(schedule.getCpfClient())).child("schedules").child(id).setValue(schedule);
            emitter.onComplete();
        });
    }

    public Single<Schedule> getSchedule(String salonID, String scheduleID) {
        return Single.create(emitter -> {
            reference.child(salonID).child(scheduleID).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null) {
                        Schedule schedule = dataSnapshot.getValue(Schedule.class);
                        emitter.onSuccess(schedule);
                    } else {
                        emitter.onError(new Throwable());
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        });
    }

    public Single<List<Schedule>> getSchedules(String salonID) {
        return Single.create(emitter -> {
            reference.child(salonID).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null && dataSnapshot.getChildren() != null) {
                        List<Schedule> schedules = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Schedule schedule = snapshot.getValue(Schedule.class);
                            if (schedule != null && !schedule.isDeleted()) {
                                schedules.add(schedule);
                            }
                        }
                        emitter.onSuccess(schedules);
                    } else {
                        emitter.onError(new Throwable());
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        });
    }

    public Completable clearSchedule(Schedule schedule) {
        return Completable.create(emitter -> {
            database.getReference("user").child(String.valueOf(schedule.getCpfClient())).child("schedules").child(schedule.getId()).removeValue().addOnSuccessListener(aVoid -> {
                emitter.onComplete();
            });
        });
    }

    public Completable acceptSchedule(Schedule schedule) {
        schedule.setAccepted(true);
        return Completable.create(emitter -> {
            reference.child(schedule.getCnpjSalon().toString()).child(schedule.getId()).setValue(schedule)
                    .addOnSuccessListener(aVoid -> {
                        database.getReference("user").child(String.valueOf(schedule.getCpfClient())).child("schedules").child(schedule.getId()).setValue(schedule).addOnSuccessListener(aVoid1 -> {
                            emitter.onComplete();
                        });
                    });
        });
    }

    public Completable refuseSchedule(Schedule schedule) {
        schedule.setAccepted(false);
        return Completable.create(emitter -> reference.child(schedule.getCnpjSalon().toString()).child(schedule.getId()).setValue(schedule)
                .addOnSuccessListener(aVoid -> {
                    database.getReference("user").child(String.valueOf(schedule.getCpfClient())).child("schedules").child(schedule.getId()).setValue(schedule).addOnSuccessListener(aVoid1 -> {
                        emitter.onComplete();
                    });
                }));
    }

}
