package com.example.projectjakobs.util;

import android.text.Editable;
import android.text.TextWatcher;

public class MaskWatcher implements TextWatcher {
    private boolean isRunning = false;
    private boolean isDeleting = false;
    private final String mask;
    public final static String ZIPCODE_MASK = "#####-###";
    public final static String CPF_MASK = "###.###.###-##";
    public final static String CNPJ_MASK = "##.###.###/####-##";
    public final static String SMARTPHONE_MASK = "(##) #####-####";
    public final static String PHONE_MASK = "(##) ####-####";

    public MaskWatcher(String mask) {
        this.mask = mask;
    }

    public static MaskWatcher buildCpf() { return new MaskWatcher(CPF_MASK); }

    public static MaskWatcher buildSmartPhone() {
        return new MaskWatcher(SMARTPHONE_MASK);
    }

    public static TextWatcher buildPhone() {
        return new MaskWatcher(PHONE_MASK);
    }

    public static MaskWatcher buildZipcode() {
        return new MaskWatcher(ZIPCODE_MASK);
    }

    public static TextWatcher buildCnpj() {
        return new MaskWatcher(CNPJ_MASK);
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
        isDeleting = count > after;
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (isRunning || isDeleting || editable.length() == 0) {
            return;
        }
        isRunning = true;

        int editableLength = editable.length();
        if (editableLength < mask.length()) {
            if (mask.charAt(editableLength) != '#') {
                editable.append(mask.charAt(editableLength));
            } else if (mask.charAt(editableLength-1) != '#') {
                editable.insert(editableLength-1, mask, editableLength-1, editableLength);
            }
        }

        isRunning = false;
    }

    public static String addMask(final String textoAFormatar, final String mask){
        StringBuilder formatado = new StringBuilder();
        int i = 0;

        for (char m : mask.toCharArray()) {
            if (m != '#') {
                formatado.append(m);
                continue;
            }

            try {
                formatado.append(textoAFormatar.charAt(i));
            } catch (Exception e) {
                break;
            }
            i++;
        }
        return formatado.toString();
    }
}