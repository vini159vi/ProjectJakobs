package com.example.projectjakobs.util;

public class Constants {

    public enum Status {
        LOADING,
        SUCCESS,
        ERROR
    }

    public static final String USER = "USER";
    public static final String SALON = "SALON";
    public static final String CPF = "CPF";
    public static final String CNPJ = "CNPJ";

    public static final String MASCULINO = "MASCULINO";
    public static final String FEMININO = "FEMININO";
}
