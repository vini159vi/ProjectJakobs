package com.example.projectjakobs.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import com.example.projectjakobs.R;
import com.example.projectjakobs.domain.Salon;
import com.example.projectjakobs.domain.User;
import com.google.gson.Gson;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferencesUtil {

    public static void setSalon(Salon salon, Context context) {
        Gson gson = new Gson();
        String salonGson = gson.toJson(salon);
        SharedPreferences preferences = context.getSharedPreferences("jakobs", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(context.getString(R.string.salon), salonGson);
        editor.commit();
    }

    public static Salon getSalon(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("jakobs", MODE_PRIVATE);

        String salonGson = preferences.getString(context.getString(R.string.salon), "");
        Gson gson = new Gson();
        if(!salonGson.isEmpty())
            return gson.fromJson(salonGson, Salon.class);
        else
            return new Salon();

    }

    public static void setUser(User user, Context context) {
        Gson gson = new Gson();
        String userGson = gson.toJson(user);
        SharedPreferences preferences = context.getSharedPreferences("jakobs", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.USER, userGson);
        editor.commit();
    }

    public static User getUser(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("jakobs", MODE_PRIVATE);
        String userGson = preferences.getString(Constants.USER, "");
        Gson gson = new Gson();
        if(!userGson.isEmpty())
            return gson.fromJson(userGson, User.class);
        else
            return new User();

    }
}
