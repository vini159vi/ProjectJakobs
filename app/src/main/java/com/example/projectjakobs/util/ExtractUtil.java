package com.example.projectjakobs.util;

public class ExtractUtil {
    public static String transformInString(String age) {
        switch (age) {
            case "UNDER_18":
            case "under 18":
                return "<18";
            case "FROM_18_TO_20":
            case "18 to 20":
                return "18-20";
            case "FROM_21_TO_28":
            case "21 to 28":
                return "21-28";
            case "FROM_29_TO_36":
            case "29 to 36":
                return "29-36";
            case "FROM_37_TO_44":
            case "37 to 44":
                return "37-44";
            case "FROM_45_TO_52":
            case "45 to 52":
                return "45-52";
            case "FROM_53_TO_60":
            case "53 to 60":
                return "53-60";
            case "OVER_60":
            case "over 60":
                return ">60";
        }
        return "_";
    }
}
