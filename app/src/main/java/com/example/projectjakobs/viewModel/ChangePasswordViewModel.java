package com.example.projectjakobs.viewModel;

import android.view.View;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.projectjakobs.domain.Response;
import com.example.projectjakobs.repository.LoginRepository;

public class ChangePasswordViewModel extends ViewModel {
    private String cpfUser;

    public MutableLiveData<Response> response = new MutableLiveData<>();
    public String oldPassword = "";
    public String newPassword = "";
    public String againPassword = "";

    private LoginRepository loginRepository = new LoginRepository();

    public void init(String cpf){
        this.cpfUser = cpf;
    }
    public void changePassword(View view) {
        if (newPassword.equals(againPassword)) {
            loginRepository.changePassword(cpfUser, oldPassword, newPassword, response);
        }
    }
}
