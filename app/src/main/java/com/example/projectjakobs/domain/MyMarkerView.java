package br.com.monitoratec.gryncashandroid.util;

import android.content.Context;
import android.widget.TextView;
import com.example.projectjakobs.R;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;

import java.util.ArrayList;


public class MyMarkerView extends MarkerView {

    private final TextView tvContent;
    private final TextView tvLabel;
    private final ArrayList<String> xVals;

    public MyMarkerView(Context context, int layoutResource, ArrayList<String> xVals) {
        super(context, layoutResource);

        tvContent = findViewById(R.id.tvContent);
        tvLabel = findViewById(R.id.tvLabel);
        this.xVals = xVals;
    }

    // runs every time the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        if (e instanceof CandleEntry) {

            CandleEntry ce = (CandleEntry) e;
            tvContent.setText(Utils.formatNumber(ce.getHigh(), 0, true));
        } else {

            tvContent.setText(Utils.formatNumber(e.getY(), 0, true));

            if (xVals != null && xVals.get((int) e.getX()) != null) {
                tvLabel.setText(xVals.get((int) e.getX()));
                tvLabel.setVisibility(VISIBLE);
            }
        }

        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}