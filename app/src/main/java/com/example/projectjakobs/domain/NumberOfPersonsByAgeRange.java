package com.example.projectjakobs.domain;

public class NumberOfPersonsByAgeRange {
    private int numberOfPersons;
    private int age = 0;

    public NumberOfPersonsByAgeRange() {

    }

    public NumberOfPersonsByAgeRange(int numberOfPersons, int ageRange) {
        this.numberOfPersons = numberOfPersons;
        this.age = ageRange;
    }

    public int getNumberOfPersons() {
        return numberOfPersons;
    }

    public void setNumberOfPersons(int numberOfPersons) {
        this.numberOfPersons = numberOfPersons;
    }

    public int getAgeRange() {
        return age;
    }

    public void setAgeRange(int gender) {
        this.age = gender;
    }
}
