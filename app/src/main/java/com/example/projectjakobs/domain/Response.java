package com.example.projectjakobs.domain;

import static com.example.projectjakobs.util.Constants.*;
import static com.example.projectjakobs.util.Constants.Status.ERROR;
import static com.example.projectjakobs.util.Constants.Status.LOADING;
import static com.example.projectjakobs.util.Constants.Status.SUCCESS;

public class Response {

    public final Status status;


    public final Object data;


    public final Throwable error;

    private Response(Status status, Object data, Throwable error) {
        this.status = status;
        this.data = data;
        this.error = error;
    }

    public static Response loading() {
        return new Response(LOADING, null, null);
    }

    public static Response success(Object data) {
        return new Response(SUCCESS, data, null);
    }

    public static Response error(Throwable error) {
        return new Response(ERROR, null, error);
    }

}
