package com.example.projectjakobs.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Schedule implements Parcelable {
    private String id;
    private Long dateIssue;
    private Long dateMarked;
    private boolean deleted;
    private Boolean accepted;
    private Long cpfClient;
    private String nameClient;
    private Long cnpjSalon;
    private String nameSalon;
    private int ageClient;
    private String genderClient;
    private String salonPhotoUrl;
    private String salonPhone;

    public Schedule() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCnpjSalon() {
        return cnpjSalon;
    }

    public void setCnpjSalon(Long cnpjSalon) {
        this.cnpjSalon = cnpjSalon;
    }

    public String getNameSalon() {
        return nameSalon;
    }

    public void setNameSalon(String nameSalon) {
        this.nameSalon = nameSalon;
    }

    public Long getCpfClient() {
        return cpfClient;
    }

    public void setCpfClient(Long cpfClient) {
        this.cpfClient = cpfClient;
    }

    public String getNameClient() {
        return nameClient;
    }

    public void setNameClient(String nameClient) {
        this.nameClient = nameClient;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public Long getDateIssue() {
        return dateIssue;
    }

    public void setDateIssue(Long dateIssue) {
        this.dateIssue = dateIssue;
    }

    public Long getDateMarked() {
        return dateMarked;
    }

    public void setDateMarked(Long dateMarked) {
        this.dateMarked = dateMarked;
    }

    public int getAgeClient() {
        return ageClient;
    }

    public void setAgeClient(int ageClient) {
        this.ageClient = ageClient;
    }

    public String getGenderClient() {
        return genderClient;
    }

    public void setGenderClient(String genderClient) {
        this.genderClient = genderClient;
    }

    public String getSalonPhotoUrl() {
        return salonPhotoUrl;
    }

    public void setSalonPhotoUrl(String salonPhotoUrl) {
        this.salonPhotoUrl = salonPhotoUrl;
    }

    public String getSalonPhone() {
        return salonPhone;
    }

    public void setSalonPhone(String salonPhone) {
        this.salonPhone = salonPhone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeValue(this.dateIssue);
        dest.writeValue(this.dateMarked);
        dest.writeByte(this.deleted ? (byte) 1 : (byte) 0);
        dest.writeValue(this.accepted);
        dest.writeValue(this.cpfClient);
        dest.writeString(this.nameClient);
        dest.writeValue(this.cnpjSalon);
        dest.writeString(this.nameSalon);
        dest.writeInt(this.ageClient);
        dest.writeString(this.genderClient);
        dest.writeString(this.salonPhotoUrl);
    }

    protected Schedule(Parcel in) {
        this.id = in.readString();
        this.dateIssue = (Long) in.readValue(Long.class.getClassLoader());
        this.dateMarked = (Long) in.readValue(Long.class.getClassLoader());
        this.deleted = in.readByte() != 0;
        this.accepted = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.cpfClient = (Long) in.readValue(Long.class.getClassLoader());
        this.nameClient = in.readString();
        this.cnpjSalon = (Long) in.readValue(Long.class.getClassLoader());
        this.nameSalon = in.readString();
        this.ageClient = in.readInt();
        this.genderClient = in.readString();
        this.salonPhotoUrl = in.readString();
    }

    public static final Creator<Schedule> CREATOR = new Creator<Schedule>() {
        @Override
        public Schedule createFromParcel(Parcel source) {
            return new Schedule(source);
        }

        @Override
        public Schedule[] newArray(int size) {
            return new Schedule[size];
        }
    };
}
