package com.example.projectjakobs.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class Address implements Parcelable {
    private String zipcode;
    private String city;
    private String state;
    private String street;
    private String number;
    private String neighborhood;
    private String complement;

    public Address() {
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.zipcode);
        dest.writeString(this.city);
        dest.writeString(this.state);
        dest.writeString(this.street);
        dest.writeString(this.number);
        dest.writeString(this.neighborhood);
        dest.writeString(this.complement);
    }

    protected Address(Parcel in) {
        this.zipcode = in.readString();
        this.city = in.readString();
        this.state = in.readString();
        this.street = in.readString();
        this.number = in.readString();
        this.neighborhood = in.readString();
        this.complement = in.readString();
    }

    public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel source) {
            return new Address(source);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };
}
