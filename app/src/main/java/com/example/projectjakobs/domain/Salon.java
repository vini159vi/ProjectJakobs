package com.example.projectjakobs.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

public class Salon implements Parcelable {
    private String name;
    private String description;
    private String category;
    private String cnpj;
    private String photoUrl;
    private String phone;
    private String email;
    private HashMap<String, Double> serviceAndPrice;
    private Address address;
    private String addressString;

    public Salon() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public HashMap<String, Double> getServiceAndPrice() {
        return serviceAndPrice;
    }

    public void setServiceAndPrice(HashMap<String, Double> serviceAndPrice) {
        this.serviceAndPrice = serviceAndPrice;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getAddressString() {
        return addressString;
    }

    public void setAddressString(String addressString) {
        this.addressString = addressString;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.category);
        dest.writeString(this.cnpj);
        dest.writeString(this.photoUrl);
        dest.writeString(this.phone);
        dest.writeString(this.email);
        dest.writeSerializable(this.serviceAndPrice);
        dest.writeParcelable(this.address, flags);
    }

    protected Salon(Parcel in) {
        this.name = in.readString();
        this.description = in.readString();
        this.category = in.readString();
        this.cnpj = in.readString();
        this.photoUrl = in.readString();
        this.phone = in.readString();
        this.email = in.readString();
        this.serviceAndPrice = (HashMap<String, Double>) in.readSerializable();
        this.address = in.readParcelable(Address.class.getClassLoader());
    }

    public static final Parcelable.Creator<Salon> CREATOR = new Parcelable.Creator<Salon>() {
        @Override
        public Salon createFromParcel(Parcel source) {
            return new Salon(source);
        }

        @Override
        public Salon[] newArray(int size) {
            return new Salon[size];
        }
    };
}
