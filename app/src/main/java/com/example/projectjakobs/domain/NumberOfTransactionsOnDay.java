package com.example.projectjakobs.domain;

public class NumberOfTransactionsOnDay {
    private int numberOfTransactions;
    private long day = 0L;

    public NumberOfTransactionsOnDay() {

    }
    public NumberOfTransactionsOnDay(int numberOfTransactions, long day) {
        this.numberOfTransactions = numberOfTransactions;
        this.day = day;
    }

    public int getNumberOfTransactions() {
        return numberOfTransactions;
    }

    public void setNumberOfTransactions(int numberOfTransactions) {
        this.numberOfTransactions = numberOfTransactions;
    }

    public long getDay() {
        return day;
    }

    public void setDay(long day) {
        this.day = day;
    }
}
