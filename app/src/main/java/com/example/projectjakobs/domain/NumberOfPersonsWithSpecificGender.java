package com.example.projectjakobs.domain;

public class NumberOfPersonsWithSpecificGender {
    private int numberOfPersons;
    private String gender = "";

    public NumberOfPersonsWithSpecificGender() {

    }

    public NumberOfPersonsWithSpecificGender(int numberOfPersons, String gender) {
        this.numberOfPersons = numberOfPersons;
        this.gender = gender;
    }

    public int getNumberOfPersons() {
        return numberOfPersons;
    }

    public void setNumberOfPersons(int numberOfPersons) {
        this.numberOfPersons = numberOfPersons;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
