package com.example.projectjakobs.impl;

import com.example.projectjakobs.domain.Schedule;
import com.example.projectjakobs.repository.ScheduleRepository;
import com.example.projectjakobs.repository.UserRepository;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import java.util.List;

public class ScheduleUserImpl {
    private UserRepository userRepository = new UserRepository();

    public ScheduleUserImpl() {
    }

    public Single<List<Schedule>> getSchedules(String userID) {
        return userRepository.getUserSchedules(userID)
                .subscribeOn(Schedulers.computation());
    }
}
