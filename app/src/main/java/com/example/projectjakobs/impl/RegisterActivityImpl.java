package com.example.projectjakobs.impl;

import android.app.Activity;
import android.content.Intent;

import com.example.projectjakobs.domain.User;
import com.example.projectjakobs.ui.activities.RegisterPeopleActivity;

import static com.example.projectjakobs.util.Constants.USER;

public class RegisterActivityImpl {

    Activity view;

    public RegisterActivityImpl(Activity activity) {
        this.view = activity;
    }

    public void next(User user){
        Intent intent = new Intent(view, RegisterPeopleActivity.class);
        intent.putExtra(USER, user);
        view.startActivity(intent);
    }

}
