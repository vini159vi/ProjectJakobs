package com.example.projectjakobs.impl;

import android.app.Activity;
import com.example.projectjakobs.domain.Login;
import com.example.projectjakobs.domain.Salon;
import com.example.projectjakobs.domain.User;
import com.example.projectjakobs.repository.LoginRepository;
import com.example.projectjakobs.repository.SalonRepository;
import com.example.projectjakobs.repository.UserRepository;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class LoginActivityImpl {

    private Activity view;
    private LoginRepository loginRepository = new LoginRepository();
    private SalonRepository salonRepository = new SalonRepository();
    private UserRepository userRepository = new UserRepository();

    public LoginActivityImpl(Activity activity) {
        this.view = activity;
    }

    public Single<Login> login(Login login){
        return loginRepository.login(login)
                .subscribeOn(Schedulers.computation());
    }

    public Single<User> getUser(String cpf){
        return userRepository.getUser(cpf).subscribeOn(Schedulers.computation());

    }

    public Single<Salon> getSalon(String cnpj){
        return salonRepository.getSalon(cnpj).subscribeOn(Schedulers.computation());

    }
}
