package com.example.projectjakobs.impl;

import android.app.Activity;
import com.example.projectjakobs.domain.Login;
import com.example.projectjakobs.domain.User;
import com.example.projectjakobs.repository.LoginRepository;
import com.example.projectjakobs.repository.UserRepository;
import com.example.projectjakobs.util.Constants;
import com.example.projectjakobs.util.SharedPreferencesUtil;

import java.io.File;

import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class RegisterPeopleImpl {

    Activity activity;
    private UserRepository userRepository = new UserRepository();
    private LoginRepository loginRepository = new LoginRepository();

    public RegisterPeopleImpl(Activity activity) {
        this.activity = activity;
    }

    public Completable registerUser(User user, File file){
        Login login = new Login();
        login.setLogin(user.getCpf());
        login.setPassword(user.getPassword());
        login.setUserType(Constants.CPF);
        user.setPassword(null);

        if(file == null) {
            return Completable.concatArray(loginRepository.setNewAccount(login), userRepository.registerUser(user))
                    .subscribeOn(Schedulers.computation())
                    .doOnComplete(() -> SharedPreferencesUtil.setUser(user, activity));
        } else {
            return Completable.concatArray(loginRepository.setNewAccount(login), userRepository.registerUser(user, file))
                    .subscribeOn(Schedulers.computation())
                    .doOnComplete(() -> SharedPreferencesUtil.setUser(user, activity));
        }
    }

}
