package com.example.projectjakobs.impl;

import android.content.Context;
import com.example.projectjakobs.domain.Salon;
import com.example.projectjakobs.repository.SalonRepository;

import com.example.projectjakobs.util.SharedPreferencesUtil;
import io.reactivex.Single;

public class ProfileFragmentImpl{
    SalonRepository salonRepository = new SalonRepository();
    Context context;

    public ProfileFragmentImpl(Context context) {
        this.context = context;
    }

    public Salon getSalon(){
        return SharedPreferencesUtil.getSalon(context);
    }
}
