package com.example.projectjakobs.impl;

import com.example.projectjakobs.domain.NumberOfPersonsByAgeRange;
import com.example.projectjakobs.domain.NumberOfPersonsWithSpecificGender;
import com.example.projectjakobs.domain.NumberOfTransactionsOnDay;
import com.example.projectjakobs.domain.Schedule;
import com.example.projectjakobs.repository.ScheduleRepository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class ExtractFragmnetImpl {
    private ScheduleRepository scheduleRepository = new ScheduleRepository();
    private final long SEVEN_DAYS_IN_TIME_IN_MILIS = 604800000L;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public Single<List<Object>> getSchedules(String salonID) {
        return scheduleRepository.getSchedules(salonID)
                .subscribeOn(Schedulers.computation())
                .map(schedules -> {
                    List<Object> schedulesData = new ArrayList<>();
                    Date today = new Date(System.currentTimeMillis());
                    today.setHours(0);
                    today.setMinutes(0);
                    today.setSeconds(0);
                    Date sevenDaysOld = new Date(today.getTime() - SEVEN_DAYS_IN_TIME_IN_MILIS);


                    HashMap<String, Integer> numberOfTransactionsOnDayHash = new HashMap<>();
                    HashMap<Integer, Integer> numberOfPersonsByAgeRangeHash = new HashMap<>();
                    HashMap<String, Integer> numberOfPersonsWithSpecificGenderHash = new HashMap<>();
                    HashMap<Long, Boolean> clientsValid = new HashMap<>();

                    for (Schedule schedule : schedules) {
                        if (new Date(schedule.getDateMarked()).after(sevenDaysOld)) {
                            Date date = new Date(schedule.getDateMarked());
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            date.setHours(0);
                            date.setMinutes(0);
                            date.setSeconds(0);

                            if(numberOfTransactionsOnDayHash.get(sdf.format(date)) != null) {
                                numberOfTransactionsOnDayHash.put(sdf.format(date), numberOfTransactionsOnDayHash.get(sdf.format(date)) + 1);
                            } else {
                                numberOfTransactionsOnDayHash.put(sdf.format(date), 1);
                            }

                            if(clientsValid.get(schedule.getCpfClient()) == null){
                                if (numberOfPersonsByAgeRangeHash.get(schedule.getAgeClient()) != null) {
                                    numberOfPersonsByAgeRangeHash.put(schedule.getAgeClient(), numberOfPersonsByAgeRangeHash.get(schedule.getAgeClient()) + 1);
                                } else {
                                    numberOfPersonsByAgeRangeHash.put(schedule.getAgeClient(), 1);
                                }

                                if (numberOfPersonsWithSpecificGenderHash.get(schedule.getGenderClient()) != null) {
                                    numberOfPersonsWithSpecificGenderHash.put(schedule.getGenderClient(), numberOfPersonsWithSpecificGenderHash.get(schedule.getGenderClient()) + 1);
                                } else {
                                    numberOfPersonsWithSpecificGenderHash.put(schedule.getGenderClient(), 1);
                                }
                            }

                        }

                        clientsValid.put(schedule.getCpfClient(), true);
                    }

                    for (Map.Entry<String, Integer> entry :numberOfTransactionsOnDayHash.entrySet()) {
                        schedulesData.add(new NumberOfTransactionsOnDay(entry.getValue(), sdf.parse(entry.getKey()).getTime()));
                    }
                    for (Map.Entry<Integer, Integer> entry :numberOfPersonsByAgeRangeHash.entrySet()) {
                        schedulesData.add(new NumberOfPersonsByAgeRange(entry.getValue(), entry.getKey()));
                    }
                    for (Map.Entry<String, Integer> entry :numberOfPersonsWithSpecificGenderHash.entrySet()) {
                        schedulesData.add(new NumberOfPersonsWithSpecificGender(entry.getValue(), entry.getKey()));
                    }

                    return schedulesData;
                });
    }
}
