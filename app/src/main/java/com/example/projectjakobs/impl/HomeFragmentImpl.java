package com.example.projectjakobs.impl;

import android.content.Context;

import com.example.projectjakobs.domain.Salon;
import com.example.projectjakobs.repository.SalonRepository;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class HomeFragmentImpl {
    private Context context;
    private SalonRepository salonRepository = new SalonRepository();

    public HomeFragmentImpl(Context context) {
        this.context = context;
    }

    public Single<List<Salon>> getSalons(){
        return salonRepository.getSalons()
                .subscribeOn(Schedulers.computation());

    }
}
