package com.example.projectjakobs.impl;

import android.content.Context;
import com.example.projectjakobs.domain.Login;
import com.example.projectjakobs.domain.Salon;
import com.example.projectjakobs.repository.LoginRepository;
import com.example.projectjakobs.repository.SalonRepository;
import com.example.projectjakobs.util.Constants;
import com.example.projectjakobs.util.SharedPreferencesUtil;

import java.io.File;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;

public class RegisterEstablishmentImpl {

    private Context context;

    public RegisterEstablishmentImpl(Context context) {
        this.context = context;
    }

    public Completable setNewSalon(Salon newSalon, String password, File file) {
        SalonRepository repository = new SalonRepository();
        LoginRepository loginRepository = new LoginRepository();
        Login login = new Login();
        login.setLogin(newSalon.getCnpj());
        login.setPassword(password);
        login.setUserType(Constants.CNPJ);

        if(file == null) {
            return Completable.concatArray(loginRepository.setNewAccount(login), repository.setNewSalon(newSalon))
                    .subscribeOn(Schedulers.computation())
                    .doOnComplete(() -> SharedPreferencesUtil.setSalon(newSalon, context));

        } else {
            return Completable.concatArray(loginRepository.setNewAccount(login), repository.setNewSalon(newSalon, file))
                    .subscribeOn(Schedulers.computation())
                    .doOnComplete(() -> SharedPreferencesUtil.setSalon(newSalon, context));
        }
    }
}
