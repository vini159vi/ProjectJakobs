package com.example.projectjakobs.impl;

import com.example.projectjakobs.domain.Schedule;
import com.example.projectjakobs.repository.ScheduleRepository;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import java.util.List;

public class ScheduleSalonImpl {

    private ScheduleRepository scheduleRepository = new ScheduleRepository();

    public ScheduleSalonImpl() {
    }

    public Single<List<Schedule>> getSchedules(String salonID) {
        return scheduleRepository.getSchedules(salonID)
                .subscribeOn(Schedulers.computation());
    }
}
